# Prisma

Start the project (yarn or npm):

```bash
yarn install
yarn start
```

# Structure of the project

- The css part is located at `src/css` and use sass preprocessing, organized into 5 sections:

  - The main folder contain the style of the main section `<main/>`
  - The header folder contain the style of the header section `<header/>`
  - The footer folder contain the style of the footer section `<footer>`
  - The config folder contain common styles (sizes, fonts, media-query, colors..)
  - The base folder is for CSS resetting and utility classes

- All scripts are located at `src/scripts`

- HTLM file is located at `public/index.html`

# Answering some important questions

- Why the scripts use Typescript?

Type safety is not a big deal at the scale of this project, but I still prefer to use TS so I can enable all Vscode refactoring and referencing features, plus the extra safety hints are worthwhile.

- Why using Snowpack instead of Webpack?

Since the project is not about configuration skills, I thought I can use a tool that provide everything I need out of the box (live reloading, TS support, build optimization...)

- Why CSS classes starts with a capital letter?

I drifted a bit from the general roles of BEM, so I can enable framework fallback, every class with capital letter reference a component, this will make it easy to refactor the code to use a library like React or Vue in the future

- Why I'm using a functional imperative code instead of OOP?

I thougth there's no need to use OOP, the scripts fits very well in few functions with plain imperative code

# What I think could be done better

- I think it is important to add integration tests, reflecting the user interaction with the interface with something like 'testing-library' and Jest

- Using an utility first classe library like tailwindcss

# Parallax script

- to add parallax to an image add 'parallax-element' to its classes
- to add parallax to a background add 'parallax-container' to its classes

# THE END

Thank you for taking the time to read my code, if you have any question please feel free to contact me via email or my phone. If you find anything that could be done better I would be happy to discuss it with you. Finally, I want to say that I had fun working on this, I haven't done plain HTML and CSS since 2011 and this was very fun experience.
