/**
 * DOM manipulation
 *
 **/

import stickyNav from './scripts/sticky-nav';
import parallax from './scripts/parallax';

// Scroll event
window.onscroll = () => {
  stickyNav();
};

let ticking = false;

document.addEventListener('scroll', () => {
  if (!ticking) {
    window.requestAnimationFrame(() => {
      parallax();
      ticking = false;
    });

    ticking = true;
  }
});
