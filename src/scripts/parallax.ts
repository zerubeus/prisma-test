// Parallax util

// Find element with 'parallax-container' class
const parallaxContainers = Array.from(
  document.getElementsByClassName('parallax-container') as HTMLCollectionOf<
    HTMLElement
  >,
);

// FInd element with 'parallax-element' class
const parallaxElement = Array.from(
  document.getElementsByClassName('parallax-element') as HTMLCollectionOf<
    HTMLElement
  >,
);

// Utility function to apply translation to a given DOM element
function translate(
  element: HTMLElement,
  translateElement: number,
  speed: number,
) {
  element.style.transform = 'translateY(' + translateElement * +speed + 'px)';
}

// Utility function to apply style to background position
function changeBackgroundPosition(
  container: HTMLElement,
  translationValue: number,
  speed: number,
) {
  container.style.backgroundPosition = '0px ' + translationValue * speed + 'px';
}

// Get DOM elements and data-speed attribute, then compute distance in given position of the screen
function getDomElelentAndApllyStyleChange(
  element: HTMLElement,
  styleFunction: Function,
) {
  const elementY = element.offsetTop;
  const windowY = window.scrollY;
  const windowHieght = window.innerHeight;

  const speed = element.getAttribute('data-speed') || 0;

  // height of the element with 'parallax-element' class
  const elementHeight = +window
    .getComputedStyle(element, null)
    .getPropertyValue('height')
    .split('px')[0];

  // Distance from top screen to bottom
  const windowBottom = windowY + windowHieght;

  let translateElement: number;

  // check if the element is visible in the screen before applying parallax
  if (windowBottom > elementY && windowY < elementY + elementHeight) {
    translateElement = Math.max(1, windowY - elementY);

    styleFunction(element, translateElement, +speed);
  }
}

// Apply parallax to all elements with 'parallax-container' and 'parallax-element' classes
function parallaxAplly() {
  if (parallaxContainers.length > 0) {
    parallaxContainers.map((container) => {
      getDomElelentAndApllyStyleChange(container, changeBackgroundPosition);
    });
  }

  if (parallaxElement.length > 0) {
    parallaxElement.map((element) => {
      getDomElelentAndApllyStyleChange(element, translate);
    });
  }
}

export default parallaxAplly;
