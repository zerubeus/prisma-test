// Apply a second css class to .MainNavigation when scrolling down reach 10

const nav = document.querySelector('.MainNavigation');

function fixOnScroll() {
  if (window.scrollY >= 10 && nav) {
    nav.classList.add('Nav-on-scroll');
  } else {
    if (nav) nav.classList.remove('Nav-on-scroll');
  }
}

export default fixOnScroll;
